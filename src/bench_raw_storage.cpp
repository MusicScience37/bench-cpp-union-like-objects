#include <cstdint>
#include <new>
#include <vector>

#include <stat_bench/benchmark_macros.h>

namespace bench_raw_storage {

constexpr std::size_t max_align =
    std::max(alignof(std::uint32_t), alignof(float));
constexpr std::size_t max_size = std::max(sizeof(std::uint32_t), sizeof(float));

struct Storage {
    alignas(max_align) char storage[max_size];
};

}  // namespace bench_raw_storage

STAT_BENCH_CASE("create_union_like_objects", "raw_storage") {
    const std::size_t data_size = STAT_BENCH_CONTEXT_NAME.iterations();
    std::vector<bench_raw_storage::Storage> vec;
    vec.reserve(data_size);
    STAT_BENCH_MEASURE_INDEXED(/*thread_ind*/, /*sample_ind*/, i) {
        if (i == 0) {
            vec.clear();
        }
        bench_raw_storage::Storage data{};
        if (i % 2U == 0) {
            new (data.storage) std::uint32_t(static_cast<std::uint32_t>(i));
        } else {
            new (data.storage) float(static_cast<float>(i));
        }
        vec.push_back(data);
    };
}

STAT_BENCH_CASE("access_union_like_objects", "raw_storage") {
    const std::size_t data_size = STAT_BENCH_CONTEXT_NAME.iterations();
    std::vector<bench_raw_storage::Storage> vec;
    vec.reserve(data_size);
    for (std::size_t i = 0; i < data_size; ++i) {
        bench_raw_storage::Storage data{};
        if (i % 2U == 0) {
            new (data.storage) std::uint32_t(static_cast<std::uint32_t>(i));
        } else {
            new (data.storage) float(static_cast<float>(i));
        }
        vec.push_back(data);
    }

    STAT_BENCH_MEASURE_INDEXED(/*thread_ind*/, /*sample_ind*/, i) {
        if (i % 2U == 0) {
            *std::launder(reinterpret_cast<std::uint32_t*>(
                static_cast<char*>(vec[i].storage))) += 1;
        } else {
            *std::launder(reinterpret_cast<float*>(
                static_cast<char*>(vec[i].storage))) += 1.0F;
        }
    };
}
