#include <cstdint>
#include <type_traits>
#include <variant>
#include <vector>

#include <stat_bench/benchmark_macros.h>

STAT_BENCH_CASE("create_union_like_objects", "variant") {
    const std::size_t data_size = STAT_BENCH_CONTEXT_NAME.iterations();
    std::vector<std::variant<std::uint32_t, float>> vec;
    vec.reserve(data_size);
    STAT_BENCH_MEASURE_INDEXED(/*thread_ind*/, /*sample_ind*/, i) {
        if (i == 0) {
            vec.clear();
        }
        std::variant<std::uint32_t, float> data{};
        if (i % 2U == 0) {
            data = static_cast<std::uint32_t>(i);
        } else {
            data = static_cast<float>(i);
        }
        vec.push_back(data);
    };
}

STAT_BENCH_CASE("access_union_like_objects", "variant") {
    const std::size_t data_size = STAT_BENCH_CONTEXT_NAME.iterations();
    std::vector<std::variant<std::uint32_t, float>> vec;
    vec.reserve(data_size);
    for (std::size_t i = 0; i < data_size; ++i) {
        std::variant<std::uint32_t, float> data{};
        if (i % 2U == 0) {
            data = static_cast<std::uint32_t>(i);
        } else {
            data = static_cast<float>(i);
        }
        vec.push_back(data);
    }

    STAT_BENCH_MEASURE_INDEXED(/*thread_ind*/, /*sample_ind*/, i) {
        std::visit(
            [](auto& val) {
                val += static_cast<std::decay_t<decltype(val)>>(1);
            },
            vec[i]);
    };
}
