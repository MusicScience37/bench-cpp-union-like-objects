#include <cstdint>
#include <vector>

#include <stat_bench/benchmark_macros.h>

namespace bench_union {

union Data {
    std::uint32_t value_uint;
    float value_float;
};

}  // namespace bench_union

STAT_BENCH_CASE("create_union_like_objects", "union") {
    const std::size_t data_size = STAT_BENCH_CONTEXT_NAME.iterations();
    std::vector<bench_union::Data> vec;
    vec.reserve(data_size);
    STAT_BENCH_MEASURE_INDEXED(/*thread_ind*/, /*sample_ind*/, i) {
        if (i == 0) {
            vec.clear();
        }
        bench_union::Data data{};
        if (i % 2U == 0) {
            data.value_uint = static_cast<std::uint32_t>(i);
        } else {
            data.value_float = static_cast<float>(i);
        }
        vec.push_back(data);
    };
}

STAT_BENCH_CASE("access_union_like_objects", "union") {
    const std::size_t data_size = STAT_BENCH_CONTEXT_NAME.iterations();
    std::vector<bench_union::Data> vec;
    vec.reserve(data_size);
    for (std::size_t i = 0; i < data_size; ++i) {
        bench_union::Data data{};
        if (i % 2U == 0) {
            data.value_uint = static_cast<std::uint32_t>(i);
        } else {
            data.value_float = static_cast<float>(i);
        }
        vec.push_back(data);
    }

    STAT_BENCH_MEASURE_INDEXED(/*thread_ind*/, /*sample_ind*/, i) {
        if (i % 2U == 0) {
            vec[i].value_uint += 1;
        } else {
            vec[i].value_float += 1.0F;
        }
    };
}
