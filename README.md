# bench-cpp-union-like-objects

Benchmark of union-like objects in C++.

## Latest results

```txt
Benchmark start at 2022-06-03T00:11:25.155819+0000

Time resolution: 1.000e-09 sec.

## Processing Time

### access_union_like_objects

                                                   Iterations  Samples    Mean [ms]     Max [ms] Custom Outputs (mean)
------------------------------------------------------------------------------------------------------------------------
union (threads=1)                                           1      100    2.792e-05    4.800e-05
variant (threads=1)                                         1      100    2.970e-05    6.400e-05
raw_storage (threads=1)                                     1      100    2.926e-05    5.700e-05
### create_union_like_objects

                                                   Iterations  Samples    Mean [ms]     Max [ms] Custom Outputs (mean)
------------------------------------------------------------------------------------------------------------------------
union (threads=1)                                           1      100    3.117e-05    4.900e-05
variant (threads=1)                                         1      100    3.133e-05    5.800e-05
raw_storage (threads=1)                                     1      100    3.134e-05    5.500e-05

## Mean Processing Time

### access_union_like_objects

                                                   Iterations  Samples    Mean [ms]     Max [ms] Custom Outputs (mean)
------------------------------------------------------------------------------------------------------------------------
union (threads=1)                                     1000000       30    2.564e-06    2.718e-06
variant (threads=1)                                   1000000       30    7.084e-06    7.503e-06
raw_storage (threads=1)                               1000000       30    2.596e-07    2.945e-07
### create_union_like_objects

                                                   Iterations  Samples    Mean [ms]     Max [ms] Custom Outputs (mean)
------------------------------------------------------------------------------------------------------------------------
union (threads=1)                                     1000000       30    3.193e-06    3.408e-06
variant (threads=1)                                   1000000       30    3.550e-06    3.927e-06
raw_storage (threads=1)                               1000000       30    3.115e-06    3.185e-06

Benchmark finished at 2022-06-03T00:11:26.362983+0000
```
